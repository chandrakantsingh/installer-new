#!/usr/bin/env python3

import subprocess
import os

def preinstall_check_valid(boot_device):
	if os.path.isdir('/sys/firmware/efi'):
		# We have boot in UEFI Mode
		cmd = 'sudo fdisk -l ' + boot_device + ' | grep "Disklabel type"'
		result = subprocess.getoutput(cmd)
		if result.find('gpt') != -1:
			return True
		else:
			return False
	else:
		# We have boot in legacy Mode
		cmd = 'sudo fdisk -l ' + boot_device + ' | grep "Disklabel type"'
		result = subprocess.getoutput(cmd)
		if result.find('dos') != -1:
			return True
		else:
			return False

